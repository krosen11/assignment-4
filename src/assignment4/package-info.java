/**
 * This package provides the classes and methods necessary to test search
 * and sort algorithms for speed and efficiency
 * 
 * @author Kevin Rosen - KWR357
 *
 * @version 1.0
 */
package assignment4;