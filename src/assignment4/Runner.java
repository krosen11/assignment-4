/**
 * File contains Runner Class
 */
package assignment4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Driver file for the testing suite. Running this runs tests 1-5 for searching and
 * sorting an ArrayList and LinkedList (more info in Experiment.java)
 * EE422c
 * T/TH 2:00 - 3:30, W 10:30 - 12:00
 * 
 * @author Kevin Rosen - KWR357
 * @version 1.0 2014-11-2
 */
public class Runner {

	public static void main(String[] args)
	{
		Random numGen = new Random(15485863);
		Experiment test = new Experiment();
		
		for (int i = 0; i < Experiment.SIZE_LIST; i++)
		{
			int currNum = numGen.nextInt(Integer.MAX_VALUE) + 1;
			test.list.add(currNum);
		}
		
		test.Test1(false);
		test.Test2();
		test.Test3();
		test.Test4();
		test.Test5();
	}

}
