/**
 * File contains Experiment class
 */
package assignment4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
 * Class contains the methods to create and run search/sort tests.
 * Solves EE422c Assignment 4
 * 
 * @author Kevin Rosen - KWR357
 * @version 1.0 2014-10-11
 */
public class Experiment {
	
	public static final int SIZE_LIST = 1000000;
	public ArrayList<Integer> list;
	public LinkedList<Integer> linked;
	private StopWatch stopwatch;
	
	/**
	 * Constructor for an Experiment object
	 */
	public Experiment()
	{
		list = new ArrayList<Integer>();
		stopwatch = new StopWatch();
	}
	
	/**
	 * First test to run. This test searches an ArrayList for values from
	 * input.txt using a linear search. Prints the result of each search, the
	 * time it took to search the ArrayList, and how many total values found.
	 * 
	 * @param linkedList whether we are looking at a LinkedList (true) or
	 * an ArrayList(false)(this matters for Test2).
	 */
	public void Test1 (boolean linkedList)
	{
		int numFound = 0;
		try
		{
			FileReader inputFile = new FileReader("input.txt");
			BufferedReader input = new BufferedReader(inputFile);
			String line = input.readLine();
			
			while (line != null)
			{
				//We are now making sure we have a valid input number
				int searchNum = 0;
				try
				{
					searchNum = Integer.parseInt(line);
				}
				catch(NumberFormatException e)
				{
					System.out.println(line + " is not a valid integer");
					line = input.readLine();
					continue;
				}
				
				//We also need to check if the number is negative
				if (searchNum < 0) 
				{
					System.out.println("Integer needs to be greater than or equal to 0"); 
					line = input.readLine();
					continue;
				}
				
				//Now we will start the stopwatch
				stopwatch.start();
				
				linearSearch(linkedList, searchNum);
				//We have finished searching, so we stop the stopwatch
				stopwatch.stop();
				line = input.readLine();
			}
			
			System.out.println("Time spent searching: "
					+ stopwatch.getElapsedTime()/stopwatch.NANOS_PER_SEC + " seconds");
			System.out.println("Total values found: " + numFound);
			input.close();
		}
		catch (FileNotFoundException e){System.out.println("File not Found");}
		catch (IOException e){System.out.println("There was an error reading in the text.");}
		finally{stopwatch.reset();}
	}
	
	/**
	 * Second test to run. This test creates a LinkedList from our ArrayList, then
	 * runs a linear search through the LinkedList the same way that we searched
	 * the ArrayList in Test1.
	 */
	public void Test2()
	{
		linked = new LinkedList<Integer>(list);
		/*We are calling Test1 here since everything is the same between the
		 * two tests except for the data structure, which the boolean value
		 * specifies
		 */
		Test1(true);
	}
	
	/**
	 * Third test to run. Sorts the ArrayList. Prints out the time taken.
	 */
	public void Test3()
	{
		//We only want to time the sort
		stopwatch.start();
		Collections.sort(list);
		stopwatch.stop();
		System.out.println("Time spent sorting: "
				+ stopwatch.getElapsedTime()/stopwatch.NANOS_PER_SEC + " seconds");
		stopwatch.reset();
	}
	
	/**
	 * Fourth test to run. Uses the Binary Search algorithm to search the sorted
	 * ArrayList. Prints out the results of each search, total time taken to search,
	 * and how many values were found.
	 */
	public void Test4()
	{
		try
		{
			FileReader inputFile = new FileReader("input.txt");
			BufferedReader input = new BufferedReader(inputFile);
			int numFound = 0;
			String line = input.readLine();
			
			while (line != null)
			{
				//We are testing the validity of our number
				int searchNum = 0;
				try
				{
					searchNum = Integer.parseInt(line);
				}
				catch(NumberFormatException e)
				{
					System.out.println(line + " is not a valid integer");
					line = input.readLine();
					continue;
				}
				
				if (searchNum < 0) 
				{
					System.out.println("Integer needs to be greater than or equal to 0"); 
					line = input.readLine();
					continue;
				}
				
				//Using the binary search from the Java API
				stopwatch.start();
				int found = Collections.binarySearch(list, searchNum);
				stopwatch.stop();
				
				if (found >= 0)
				{
					System.out.println("Found " + searchNum + " at "
							+ "index " + found);
					numFound++;
				}
				else {System.out.println(searchNum + " not found.");}
				
				line = input.readLine();
			}
			
			System.out.println("Time spent searching: "
					+ stopwatch.getElapsedTime()/stopwatch.NANOS_PER_SEC + " seconds");
			System.out.println("Total values found: " + numFound);
			input.close();
		}
		catch (FileNotFoundException e){System.out.println("File not Found");}
		catch (IOException e){System.out.println("There was an error reading in the text.");}
		finally{stopwatch.reset();}
	}
	
	/**
	 * Fifth test to run. Uses an Interpolation Search algorithm to search the 
	 * ArrayList. Prints the results of each search, the total time taken, and 
	 * how many values were found.
	 */
	public void Test5()
	{
		try
		{
			FileReader inputFile = new FileReader("input.txt");
			BufferedReader input = new BufferedReader(inputFile);
			int numFound = 0;
			String line = input.readLine();
			
			while (line != null)
			{
				int searchNum = 0;
				try
				{
					searchNum = Integer.parseInt(line);
				}
				catch(NumberFormatException e)
				{
					System.out.println(line + " is not a valid integer");
					line = input.readLine();
					continue;
				}
				
				if (searchNum < 0) 
				{
					System.out.println("Integer needs to be greater than or equal to 0"); 
					line = input.readLine();
					continue;
				}
				
				//Calling our own Interpolation Search algorithm
				stopwatch.start();
				int index = interpolSearch(searchNum);
				stopwatch.stop();
				
				if (index == -1)
				{
					System.out.println(searchNum + " not found.");
				}
				else
				{
					System.out.println("Found " + searchNum + " at "
							+ "index " + index);
					numFound++;
				}
				
				line = input.readLine();
			}
			
			System.out.println("Time spent searching: "
					+ stopwatch.getElapsedTime()/stopwatch.NANOS_PER_SEC + " seconds");
			System.out.println("Total values found: " + numFound);
			input.close();
		}
		catch (FileNotFoundException e){System.out.println("File not Found");}
		catch (IOException e){System.out.println("There was an error reading in the text.");}
		finally{stopwatch.reset();}
	}
	
	/**
	 * Method that performs a linear search on either an ArrayList or a LinkedList.
	 * 
	 * @param linkedList specifies whether we are searching through a LinkedList
	 * or an ArrayList.
	 * @param value the number that we are searching for.
	 */
	private void linearSearch(boolean linkedList, int value)
	{
		if (!linkedList)
		{
			for (int index = 0; index < list.size(); index++)
			{
				int currNum = list.get(index);
				if (value == currNum)
				{
					System.out.println("Found " + currNum + " at "
							+ "index " + index);
					return;
				}
			}
		}
		else
		{
			int index = linked.indexOf(value);
			if (index != -1)
			{
				System.out.println("Found " + value + " at "
						+ "index " + index);
				return;
			}
		}
		
		//If we get here, then we did not find our value
		System.out.println(value + " not found.");
		return;
	}
	
	/**
	 * Method that performs the Interpolation Search.
	 * 
	 * @param value the number we are looking for.
	 * @return the index of the number we are looking for (-1 if not found).
	 */
	private int interpolSearch(int value)
	{
		int low = 0;
		int high = list.size() - 1;
		int mid;
		
		while (low < high)
		{
			mid = low + (int)Math.ceil((value - (double)list.get(low)) / 
					((double)list.get(high) - (double)list.get(low)) * 
					(high - low - 1));
			
			if (mid < 0 || mid < low || mid > high) {break;}
			if (value < list.get(mid)) {low = mid + 1;}
			else if (value > list.get(mid)) {high = mid - 1;}
			else {return mid;}
		}
		//not found
		return -1;
	}
}
